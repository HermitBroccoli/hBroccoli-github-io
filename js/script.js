'use strict';

const items = document.querySelectorAll('.conteiner-item'),
    leftBtn = document.querySelector('.conteiner-left'),
    rightBtn = document.querySelector('.conteiner-right'),
    skilsItem = document.querySelectorAll('.skils--item'),
    navScroll = document.querySelector('.pos'),
    itemNav = document.querySelectorAll('a[href*="#"]');

let numberSlide = 0;

for (let i = 0; i < skilsItem.length; i++) {
    if (i >= 3) {
        skilsItem[i].classList.add('top');
    }
}

function hideItems() {

    items.forEach(item => {
        item.classList.add('hide');
    });

    items.forEach(item => {
        item.classList.remove('active');
    });
}

function showItem(i = 0) {
    items[i].classList.remove('hide'); // удалить класс у i элемента
    items[i].classList.add('active'); // добавить класс у i элемента
}

hideItems();
showItem();

leftBtn.addEventListener('click', () => {
    numberSlide -= 1;
    if (numberSlide < 0) {
        hideItems();
        showItem(3);
        numberSlide = items.length - 1;
    } else {
        hideItems();
        showItem(numberSlide);
    }
});

rightBtn.addEventListener('click', () => {
    numberSlide += 1;
    if (numberSlide > items.length - 1) {
        hideItems();
        showItem();
        numberSlide = 0;
    } else {
        hideItems();
        showItem(numberSlide);
    }
});

for(let item of itemNav){
    item.addEventListener('click', (e) => {
        e.preventDefault();

        const blockID = item.getAttribute('href').substr(1);
        
        document.getElementById(blockID).scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });
    });
}